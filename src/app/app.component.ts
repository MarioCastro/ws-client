import { Component } from '@angular/core';
import SockJS from 'sockjs-client';
import Stomp from 'stompjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  public ws;
  public stompClient;

  constructor() {
   this.ws = new SockJS('http://localhost:8081/chat');
    this.stompClient = Stomp.over(this.ws);
    const self = this;
    this.stompClient.connect({}, function (frame) {
        // this.setConnected(true);
        console.log('Connected: ' + frame);
        console.log(self.stompClient);
        self.stompClient.subscribe('/topic/messages', function (message) {
            console.log(message);
          });
        self.stompClient.subscribe('/topic/how', function (message) {
            console.log(message);
          });

          self.stompClient.send('/app/hello' , {},  JSON.stringify({from: 'mario', text: 'hola'}));
    }, function (error) {
      console.error(error);
    });
  }



}
